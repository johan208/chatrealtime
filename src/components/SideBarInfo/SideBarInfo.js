import React from 'react';
import onlineIcon from '../../icons/onlineIcon.png';
import './SideBarInfo.css';


const SideBarInfo = ({ users }) => {
    return ( 
        <div className="textContainer">
            <div>
            <h1>Chat en Tiempo Teal <span role="img" aria-label="emoji">💬</span></h1>
            <h2>Creado Con React, Express, Node and Socket.IO <span role="img" aria-label="emoji">❤️</span></h2>
            <h2>Intentalo ahora! <span role="img" aria-label="emoji">⬅️</span></h2>
            </div>
            {
            users
                ? (
                <div>
                    <h1>Usuarios en la sala:</h1>
                    <div className="activeContainer">
                    <h2>
                        {users.map(({name}) => (
                        <div key={name} className="activeItem">
                            {name}
                            <img alt="Online Icon" src={onlineIcon}/>
                        </div>
                        ))}
                    </h2>
                    </div>
                </div>
                )
                : null
            }
        </div>
     );
}
 
export default SideBarInfo;